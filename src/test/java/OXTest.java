/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Nutcha1223
 */
public class OXTest {
    
    public OXTest() {
    }

    /*@org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }*/
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    
    //Player class
    @Test
    public void testNamePlayerO() {
        Player o = new Player('O');
        assertEquals('O',o.getName());
    }
    
    public void testNamePlayerX() {
        Player x = new Player('X');
        assertEquals('X',x.getName());
    }

    public void testGetWinPlayerO() {
        Player o = new Player('O');
        assertEquals(0,o.getWin());
    }

    public void testGetNamePlayerX() {
        Player x = new Player('X');
        assertEquals('X',x.getName());
    }

    public void testPlayerXWin() {
        Player x = new Player('X');
        x.win();
        assertEquals(1,x.getWin());
    }
    
    public void testGetLosePlayerO() {
        Player x = new Player('X');
        assertEquals(0,x.getLose());
    }
    
    
    //Table class
    public void testRow1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        try{
            table.setRowCol(1, 1);
            table.setRowCol(1, 2);
            table.setRowCol(1, 3);
        }catch(Exception e){}
        assertEquals(true,table.checkWin(0, 0));
    }
    
    public void testCol1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        try{
            table.setRowCol(3, 1);
            table.setRowCol(2,1);
            table.setRowCol(1, 1);
        }catch(Exception e){}
        assertEquals(true,table.checkWin(0, 0));
    }

    public void testDrawPass() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        try{
            table.setRowCol(1,1);
            table.setRowCol(1,2);
            table.setRowCol(1,3);
            table.setRowCol(2,1);
            table.setRowCol(2,2);
            table.setRowCol(2,3);
            table.setRowCol(3,1);
            table.setRowCol(3,2);
            table.setRowCol(3,3);
        }catch(Exception e){}
        assertEquals(true,table.checkDraw());
    }

    public void testX2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        try{
            table.setRowCol(1,3);
            table.setRowCol(2,2);
            table.setRowCol(3,1);
        }catch(Exception e){}
        assertEquals(true,table.checkWin(0, 0));
    }

    public void testSwitchTurn() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.switchTurn();
        assertEquals(x,table.getcurrentPlayer());
    }

    public void testGetPlayerWin() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        try{
            table.setRowCol(3, 1);
            table.setRowCol(2,1);
            table.setRowCol(1, 1);
        }catch(Exception e){}
        table.checkWin(0, 0);
        assertEquals(o,table.getWinner());
    }
 
}
